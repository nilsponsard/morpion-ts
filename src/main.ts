let cv = <HTMLCanvasElement>document.getElementById("cv")
let cv2 = <HTMLCanvasElement>document.getElementById("cv2")


let tailleCase = 100
let grille: Array<Array<number>> =
    [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

cv.width = tailleCase * grille.length
cv.height = tailleCase * grille[0].length
cv2.height = tailleCase
cv2.width = tailleCase
let context2 = <CanvasRenderingContext2D>cv2.getContext("2d")


let context = <CanvasRenderingContext2D>cv.getContext("2d")

let state = 0

let winner = 0
let tour: number
function init() {
    tour = 1
    winner = 0
    state = 1
    grille = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]
}
init()

cv.addEventListener("click", (e) => {
    if (state === 2) {
        init()
    }
    else if (state === 1) {
        let pos = [Math.floor(e.offsetX / tailleCase), Math.floor(e.offsetY / tailleCase)]
        if (grille[pos[0]][pos[1]] === 0) {

            ++tour
            grille[pos[0]][pos[1]] = 1 + tour % 2
            loop()

            let valid = false
            let streak = true

            for (let i = 0; i < grille.length; ++i) {
                if (grille[i][pos[1]] != 1 + tour % 2) {
                    streak = false
                    break
                }
            }
            if (streak) {
                valid = true
            }
            streak = true
            for (let j = 0; j < grille[0].length; ++j) {
                if (grille[pos[0]][j] != 1 + tour % 2) {
                    streak = false
                    break
                }

            }
            if (streak) {
                valid = true
            }
            streak = true

            if ((pos[0] + pos[1]) % 2 === 0) {
                console.log(
                    "diagonale"
                )
                for (let i = 0; i < grille.length; ++i) {
                    if (grille[i][i] != 1 + tour % 2) {
                        streak = false
                        break
                    }
                }
                if (streak) {
                    valid = true
                }
                streak = true

                for (let i = 0; i < grille.length; ++i) {
                    console.log(i, grille.length - i - 1)
                    if (grille[i][grille.length - i - 1] != 1 + tour % 2) {
                        streak = false
                        break
                    }
                }
                if (streak) {
                    valid = true
                }
            }
            if (valid) {
                winner = 1 + tour % 2
                state = 2
                console.log(`joueur ${winner} gagne`)
            }
            else if (tour === 10) {
                state = 2
                winner = 0
                console.log("égalité")
            }

        }/*
        if (grille[1][1] != 0) {
            let cas = grille[1][1]
            if (grille[0][0] === cas && grille[2][2] === cas) {
                winner = cas
                state = 2
            }
            else if (grille[2][0] === cas && grille[0][2] === cas) {
                winner = cas
                state = 2
            }
            else if (grille[1][0] === cas && grille[1][2] === cas) {
                winner = cas
                state = 2
            }
            else if (grille[0][1] === cas && grille[2][1] === cas) {
                winner = cas
                state = 2
            }
        }
        else if () {

        }*/
        //console.log(pos)


    }
    loop()
})


function loop() {
    context.clearRect(0, 0, cv.width, cv.height)
    context2.clearRect(0, 0, cv2.width, cv2.height)

    if (state === 2) {

        context.beginPath()
        context.lineWidth = 3
        let i = 0
        let j = 1
        if (winner === 1) {

            context.strokeStyle = "blue"
            context.moveTo(i * tailleCase + tailleCase / 20, j * tailleCase + tailleCase / 20)
            context.lineTo((i + 1) * tailleCase - tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
            context.moveTo((i + 1) * tailleCase - tailleCase / 20, (j) * tailleCase + tailleCase / 20)
            context.lineTo(i * tailleCase + tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
        } else if (winner === 2) {
            context.strokeStyle = "red"
            context.arc(i * tailleCase + tailleCase / 2, j * tailleCase + tailleCase / 2, tailleCase * 0.4, 0, 360)
        }
        context.stroke()
        context.closePath()
        context.strokeStyle = "black"
        context.lineWidth = 1
        context.font = `${tailleCase / 4}px monospace`
        if (winner === 0) {

            context.fillText("égalité", tailleCase * 1.2, tailleCase * 1.5)
        } else {
            context.fillText("gagne", tailleCase * 1.2, tailleCase * 1.5)

        }
    }
    else if (state === 1) {

        if ((tour + 1) % 2 + 1 === 1) {
            let i = 0
            let j = 0
            context2.beginPath()
            context2.strokeStyle = "blue"
            context2.lineWidth = 3
            context2.moveTo(i * tailleCase + tailleCase / 20, j * tailleCase + tailleCase / 20)
            context2.lineTo((i + 1) * tailleCase - tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
            context2.moveTo((i + 1) * tailleCase - tailleCase / 20, (j) * tailleCase + tailleCase / 20)
            context2.lineTo(i * tailleCase + tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
            context2.stroke()
            context2.closePath()
        }
        else if ((tour + 1) % 2 + 1 === 2) {
            let i = 0
            let j = 0
            context2.beginPath()
            context2.strokeStyle = "red"
            context2.lineWidth = 3
            //context.moveTo()
            context2.arc(i * tailleCase + tailleCase / 2, j * tailleCase + tailleCase / 2, tailleCase * 0.4, 0, 360)
            context2.stroke()
            context2.closePath()
        }
        for (let i = 0; i < grille.length; ++i) {
            for (let j = 0; j < grille[0].length; ++j) {
                context.strokeStyle = "grey"
                context.lineWidth = 1
                context.strokeRect(i * tailleCase, j * tailleCase, tailleCase, tailleCase)
                if (grille[i][j] === 1) {
                    context.beginPath()
                    context.strokeStyle = "blue"
                    context.lineWidth = 3
                    context.moveTo(i * tailleCase + tailleCase / 20, j * tailleCase + tailleCase / 20)
                    context.lineTo((i + 1) * tailleCase - tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
                    context.moveTo((i + 1) * tailleCase - tailleCase / 20, (j) * tailleCase + tailleCase / 20)
                    context.lineTo(i * tailleCase + tailleCase / 20, (j + 1) * tailleCase - tailleCase / 20)
                    context.stroke()
                    context.closePath()
                }
                if (grille[i][j] === 2) {
                    context.beginPath()
                    context.strokeStyle = "red"
                    context.lineWidth = 3
                    //context.moveTo()
                    context.arc(i * tailleCase + tailleCase / 2, j * tailleCase + tailleCase / 2, tailleCase * 0.4, 0, 360)
                    context.stroke()
                    context.closePath()
                }
            }
        }
    }


    //requestAnimationFrame(loop)
}
loop()



